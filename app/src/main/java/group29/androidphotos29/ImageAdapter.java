package group29.androidphotos29;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import group29.androidphotos29.model.User;

/**
 * Created by Samuel on 4/25/2017.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    public static int selectedPos;
    private View prevView;
    public ImageAdapter(Context c){
        mContext=c;
    }
    public int getCount(){
        return User.curAlbum.size();
    }
    public Object getItem(int position){
        return null;
    }
    public long getItemId(int position){
        return 0;
    }
    public View getView(final int position, View convertView, ViewGroup parent){
        final ImageView imageView;

        if(convertView == null){
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(200,200));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,20,8,8);
        }
        else{
            imageView = (ImageView) convertView;
        }
        imageView.setImageURI(Uri.parse(User.curAlbum.getPhotos().get(position).getUri()));
        imageView.setBackgroundResource(R.drawable.bg_key);
        imageView.setLongClickable(true);
        imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                System.out.println("ShortPress");
                Intent intent = new Intent(mContext, AlbumScreen.class);
                User.curAlbum=User.user.getAlbums().get(selectedPos);
                //intent.putExtra("album",selectedPos);
                mContext.startActivity(intent);
            }
        });
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                System.out.println("LongPress");
                if(prevView!=null) {
                    prevView.setSelected(false);
                }
                prevView=imageView;
                view.setSelected(true);
                selectedPos=position;
                //openOptionsMenu();
                return true;
            }

        });
        return imageView;
    }
}
