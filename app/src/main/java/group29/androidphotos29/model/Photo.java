/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group29.androidphotos29.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 *
 * @author Samuel
 * This class contains the file location, the date, caption, and tags
 * required for photos
 */
public class Photo implements Serializable{
    private static final long serialVersionUID = 1L;
    private String uriString;
    private HashMap<String, ArrayList<String>> tags = new HashMap();
    
    public Photo(String uri){
        this.uriString=uri;
    }
    /**
     * Get tags of photos
     * @return hashmap of tags
     */
    public HashMap<String,ArrayList<String>> getTags(){
        return tags;
    }
    /**
     * Set tags to new hashmap
     * @param tags new hashmap
     */
    public void setTags(HashMap tags){
        this.tags=tags;
    }
    /**
     * Get file of photo
     * @return file
     */
    public String getUri(){
        return uriString;
    }
    /**
     * Remove a tag from a photo
     * @param key key of tag
     * @param value value of tag
     * @return If successful
     */
    boolean removeTag(String key, String value){
        if(!tags.containsKey(key)){
            tags.remove(key);
            return true;
        }
        else{
            return false;
        }
    }
    /**
     * See if photos are equal
     * @param obj object to check
     * @return if objects are equal
     */
    @Override
    public boolean equals(Object obj){
        if(obj == null || !(obj instanceof Photo)){
            return false;
        }
        Photo tmp = (Photo)obj;
        if(tmp.uriString==this.uriString){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.uriString);
        return hash;
    }
}
