/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group29.androidphotos29.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import group29.androidphotos29.HomeScreen;

/**
 *
 * @author Samuel
 * User class to hold variables specific to the user, such as the albums, username
 * and a static variable for the current album.
 * 
 */
public class User implements Serializable{
    private static final long serialVersionUID = 1L;
    private ArrayList<Album> albums = new ArrayList();
    public static Album curAlbum;
    public static User user = new User();
    /**
     * addAlbum variation where we simply create a new empty album
     * @param name name for the album, must be unique amongst users
     * @return boolean, checking whether the album name exists or not
     */
    public boolean addAlbum(String name){
        for(Album album : albums){
            if(album.getName().equals(name)){
                return false;
            }
        }
        if(!name.isEmpty()){
            albums.add(new Album(name)); 
        }
        return true;
    }
    /**
     * addAlbum where we add an album to the user based off search results
     * @param newAlbum Album created by search results
     * @return boolean checking whether the name is unique or not
     */
    public boolean addAlbum(Album newAlbum){
        for(Album album : albums){
            if(album.getName().equals(newAlbum.getName())){
                return false;
            }
        }
        albums.add(newAlbum);
        return true;
    }
    
    public int numAlbums(){
        return albums.size();
    }
    public ArrayList<Album> getAlbums(){
        return albums;
    }
    public ArrayList<String> getAlbumNames(){
        ArrayList<String> tmp = new ArrayList();
        for(Album album : albums){
            tmp.add(album.getName());
        }
        return tmp;
    }
    /**
     * Write user data to disk using serialization
     * @throws IOException 
     */
    public static void write() throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(
            new FileOutputStream(HomeScreen.filePath + "/new.dat"));
        for(Album album : user.albums){
            for(Photo photo : album.getPhotos()){
                System.out.println("WRITE: "+photo.getUri());
            }
        }
        oos.writeObject(user);
        oos.close();
    }
    /**
     * Read user data from disk
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void read() throws IOException, ClassNotFoundException{
        File file = new File(HomeScreen.filePath+"/new.dat");
        if(!file.exists()){
            return;
        }
        ObjectInputStream ois = new ObjectInputStream(
            new FileInputStream(HomeScreen.filePath+"/new.dat"));
        user = (User)ois.readObject();
        for(Album album : user.albums){
            for(Photo photo : album.getPhotos()){
                System.out.println("READ: "+photo.getUri());
            }
        }
        ois.close();
    }
}
