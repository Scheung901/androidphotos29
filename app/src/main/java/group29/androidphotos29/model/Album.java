/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group29.androidphotos29.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Samuel
 * Album class containing the photos, album name, and the earliest and latest dates
 */
public class Album implements Serializable{
    private static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<Photo> photos = new ArrayList();
    public Album(String name){
        this.name=name;
    }
    /**
     * Get number of photos
     * @return size of the album
     */
    public int size(){
        return photos.size();
    }
    /**
     * Get the name of the album
     * @return string of name
     */
    public String getName(){
        return name;
    }
    /**
     * Set name of album
     * @param name new name
     */
    public void setName(String name){
        this.name=name;
    }
    /**
     * Get the list of photos in the album
     * @return List of photos in album
     */
    public ArrayList<Photo> getPhotos(){
        return photos;
    }
    /**
     * Check earliest and latest date to compare to the new photos date
     * @param photo 
     */
    public void addPhoto(Photo photo){
        photos.add(photo);
    }
    public void addPhoto(String uri){
        System.out.println("URI: "+uri);
        photos.add(new Photo(uri));
    }
    /**
     * When removing a photo we must check the earliest and latest dates in case
     * they have changed
     * @param position 
     */
    public void removePhoto(int position){
        photos.remove(position);
    }
    /**
     * 
     * @return Name of album
     */
    @Override
    public String toString(){
        return name;
    }
    /**
     * Equality is in name
     * @param obj Object to compare
     * @return if objects are equal
     */
    @Override
    public boolean equals(Object obj){
        if(obj == null || !(obj instanceof Album)){
            return false;
        }
        return this.name.equals(((Album)obj).name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }
}
