package group29.androidphotos29;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

import group29.androidphotos29.model.Album;
import group29.androidphotos29.model.User;

public class HomeScreen extends AppCompatActivity {
    private String newAlbumName;
    private ArrayAdapter adapter;
    private EditText input;
    int selectedPos;
    AlertDialog.Builder builder;
    ListView albumList;
    private static boolean firstTime=true;
    public static String filePath;
    public HomeScreen(){

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filePath=HomeScreen.this.getFilesDir().getPath();
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        albumList = (ListView) findViewById(R.id.albumList);
        albumList.setLongClickable(true);
        albumList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3){
                Intent intent = new Intent(HomeScreen.this, AlbumScreen.class);
                User.curAlbum=User.user.getAlbums().get(selectedPos);
                //intent.putExtra("album",selectedPos);
                startActivity(intent);
            }
        });
        albumList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view, int position, long arg3) {
                view.setSelected(true);
                selectedPos=position;
                //openOptionsMenu();
                return true;
            }

        });
        adapter = new ArrayAdapter<>(HomeScreen.this,R.layout.listview,User.user.getAlbums());
        albumList.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                builder = new AlertDialog.Builder(HomeScreen.this);
                builder.setTitle("Album Name");
                input = new EditText(HomeScreen.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        newAlbumName = input.getText().toString();
                        if(User.user.addAlbum(newAlbumName) && !newAlbumName.isEmpty()){
                            adapter.notifyDataSetChanged();
                            return;
                        }
                        else if(!newAlbumName.isEmpty()) {
                            Snackbar.make(view, "Album Exists", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            if(selectedPos<0 || selectedPos>User.user.getAlbums().size()){
                return true;
            }
            builder = new AlertDialog.Builder(HomeScreen.this);
            builder.setTitle("Album Name");
            input = new EditText(HomeScreen.this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    newAlbumName = input.getText().toString();
                    for(Album album : User.user.getAlbums()){
                        if(!album.equals(User.user.getAlbums().get(selectedPos)) && album.getName().equals(newAlbumName)) {
                            Snackbar.make(findViewById(android.R.id.content), "Album Exists", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            return;
                        }
                    }
                    if(!newAlbumName.isEmpty()){
                        User.user.getAlbums().get(selectedPos).setName(newAlbumName);
                        adapter.notifyDataSetChanged();
                        return;
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
            return true;
        }
        else if(id == R.id.action_delete){
            if(selectedPos<0 || selectedPos>User.user.getAlbums().size()){
                return true;
            }
            User.user.getAlbums().remove(selectedPos);
            adapter.notifyDataSetChanged();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(firstTime) {
            try {
                System.out.println("Reading");
                User.read();
                adapter = new ArrayAdapter<>(HomeScreen.this, R.layout.listview, User.user.getAlbums());
                albumList.setAdapter(adapter);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            firstTime=false;
        }
    }
    @Override
    public void onStop(){
        super.onStop();
        try {
            System.out.println("Writing");
            User.write();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onPause(){
        super.onPause();
        try {
            System.out.println("Writing");
            User.write();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
